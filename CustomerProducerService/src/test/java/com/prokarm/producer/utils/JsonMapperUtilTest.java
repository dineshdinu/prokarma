package com.prokarm.producer.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.prokarm.producer.domain.Address;

@ExtendWith(MockitoExtension.class)
class JsonMapperUtilTest {

  @InjectMocks
  JsonMapper helperFunctions;
  
  Address address;
  
  String result;
  
  @Spy
  JsonMapper jsonMapper;

  @BeforeEach
  public void setUp() throws JsonProcessingException {
    createAddressRequest();
    result = jsonMapper.writeValueAsString(address);
  }

  @Test
  void testObjectToJson() throws JsonProcessingException{
    when(jsonMapper.writeValueAsString(address)).thenReturn(result);
    
    assertThat(jsonMapper.writeValueAsString(address)).contains("AL1");
    assertThat(jsonMapper.writeValueAsString(address)).contains("PC");
  }


  void createAddressRequest() {
    address = new Address();
    address.setAddressLine1("AL1");
    address.setAddressLine2("AL2");
    address.setPostalCode("PC");
    address.setStreet("ST");
  }


}
