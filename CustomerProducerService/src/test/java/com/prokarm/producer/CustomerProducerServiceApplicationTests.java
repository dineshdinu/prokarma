package com.prokarm.producer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CustomerProducerServiceApplicationTests {

	@Test
	void contextLoads() {
	  assertEquals(8, "prokarma".length());
	}

}
