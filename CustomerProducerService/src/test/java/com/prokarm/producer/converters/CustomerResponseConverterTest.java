package com.prokarm.producer.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.prokarm.producer.converters.service.CustomerResponseConverterService;
import com.prokarm.producer.dto.SuccessResponse;
import com.prokarm.producer.utils.PublisherConstants;

@ExtendWith(MockitoExtension.class)
class CustomerResponseConverterTest {

  CustomerResponseConverterService converter;

  @BeforeEach
  void setUp() {
    converter = new CustomerResponseConverterService();
  }

  @Test
  void testCustomerResponseWhenItsSuccess() {
    SuccessResponse response = converter.convert(PublisherConstants.SUCCESS);
    assertEquals("success", response.getStatus());
  }
}
