package com.prokarm.producer.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.prokarm.producer.converters.service.CustomerPublisherMaskConverterService;
import com.prokarm.producer.domain.Customer;

@ExtendWith(MockitoExtension.class)
class CustomerPublisherMaskConverterTest {

  CustomerPublisherMaskConverterService converter;
  Customer customer;

  @BeforeEach
  void setUp() {
    converter = new CustomerPublisherMaskConverterService();
    createCustomerDetails();
  }

  @Test
  void testCustomerDetailsAreMasking() {
    Customer customerTest = converter.convert(customer);

    assertEquals("IN", customerTest.getCountryCode());
    assertEquals("CP345D****", customerTest.getCustomerNumber());
    assertEquals("****arma@gmail.com", customerTest.getEmail());
  }

  void createCustomerDetails() {
    customer = new Customer();
    customer.setFirstName("Prokarma");
    customer.setCustomerNumber("CP345DD890");
    customer.setEmail("prokarma@gmail.com");
    customer.setCountryCode("IN");
  }
}
