package com.prokarm.producer.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.prokarm.producer.domain.Address;
import com.prokarm.producer.domain.Customer;
import com.prokarm.producer.domain.Customer.CustomerStatusEnum;
import com.prokarm.producer.dto.SuccessResponse;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class CustomerIntegrationTest {

  @BeforeAll
  public static void setup() {
    RestAssured.baseURI = "http://localhost:8080/customer";
  }

  @Test
  void getRequest() {

    Response response = RestAssured.given().request().body(createCustomer())
        .headers(createHeaders()).auth().oauth2("87b0a835-ff7a-40f4-a0e8-64ac91af3612")
        .contentType(ContentType.JSON).when().post("/publish").then().extract().response();

    Assertions.assertEquals(200, response.statusCode());
    Assertions.assertEquals("success", response.getBody().as(SuccessResponse.class).getStatus());
  }

  Headers createHeaders() {
    Header authorizationHeader = new Header("Authorization", "some_value");
    Header transaction_IdHeader = new Header("Transaction-Id", "some_value");
    Header activity_IdHeader = new Header("Activity-Id", "some_value");

    List<Header> headers = new ArrayList<Header>();
    headers.add(activity_IdHeader);
    headers.add(transaction_IdHeader);
    headers.add(authorizationHeader);

    return new Headers(headers);
  }

  Customer createCustomer() {
    Customer customer = new Customer();
    customer.setFirstName("Prokarma Prokarma");
    customer.setLastName("Prokarma Prokarma");
    customer.setCustomerNumber("CP345DD890");
    customer.setEmail("prokarma@gmail.com");
    customer.setCountryCode("IN");
    customer.setCountry("INDIA");
    customer.setBirthdate(java.time.LocalDate.now());
    customer.setMobileNumber(1234567890);
    customer.setCustomerStatus(CustomerStatusEnum.OPEN);

    Address address = new Address();
    address.setAddressLine1("AL1");
    address.setAddressLine2("AL2");
    address.setPostalCode("PC");
    address.setStreet("ST");
    customer.setAddress(address);

    return customer;
  }
}
