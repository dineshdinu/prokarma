package com.prokarm.producer.utils;

public class PublisherConstants {

  private PublisherConstants() {

  }
  
  /*
   * Success constants and messages
   */
  public static final String SUCCESS = "success";

  
  /*
   * Error constants and messages
   */
  public static final String ERROR = "error";
  public static final String FAILED = "failed";
  public static final String ERROR_AUTHENTICATION = "Authentication exception. Please check the username/password.";
  public static final String ERROR_VALIDATION = "Please check the incoming request properties";
  public static final String ERROR_INTERNAL_SERVER_ERROR = "Error occurred on server side. Please verify logs";
  public static final String ERROR_NOT_FOUND = "Requested API is not found. Please check.";
  public static final String ERROR_KAFKA_PUBLISHER_EXCEPTION = "Error while sending the data to kafka. Please check.";
  public static final String ERROR_JSON_PARSE_EXCEPTION = "Error while parsing the object to string. Please check.";
  
  /*
   * common constants
   */
  public static final int CUSTOMER_NUMBER_MASK_DIGIT = 4;
  public static final int CUSTOMER_EMAIL_MASK_DIGIT = 4;
  public static final String MASK_FROM_START = "FromStart";
  public static final String MASK_FROM_END = "FromEnd";
}
