package com.prokarm.producer.controller;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prokarm.producer.converters.CustomerPublisherConverter;
import com.prokarm.producer.converters.CustomerPublisherMaskConverter;
import com.prokarm.producer.converters.CustomerResponseConverter;
import com.prokarm.producer.domain.Customer;
import com.prokarm.producer.dto.CustomerPublisher;
import com.prokarm.producer.dto.SuccessResponse;
import com.prokarm.producer.service.KafkaPublisher;
import com.prokarm.producer.utils.JsonMapperUtil;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

  private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

  @Autowired
  private CustomerPublisherMaskConverter maskConverter;

  @Autowired
  private CustomerPublisherConverter publisherConverter;

  @Autowired
  private KafkaPublisher kafkaPublisher;

  @Autowired
  private CustomerResponseConverter responseConverter;

  @PostMapping(value = "/publish", produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<SuccessResponse> publishCustomerDetails(
      @RequestBody @Valid Customer customer,
      @RequestHeader(required = true, value = "Authorization") String autherization,
      @RequestHeader(required = true, value = "Transaction-Id") String transactionId,
      @RequestHeader(required = true, value = "Activity-Id") String activityId) {
    String customerDetails = JsonMapperUtil.objectToJson(maskConverter.convert(customer));
    logger.info("customer details received : {}", customerDetails);
    CustomerPublisher customerPublisher = publisherConverter.convert(customer);
    String result = kafkaPublisher.publish(customerPublisher);
    SuccessResponse customerResponse = responseConverter.convert(result);
    logger.info("kafka operation result : {}", customerResponse);
    return new ResponseEntity<>(customerResponse, HttpStatus.OK);
  }
}
