package com.prokarm.producer.service;

import com.prokarm.producer.dto.CustomerPublisher;

public interface KafkaPublisher {
  public String publish(CustomerPublisher publisher);
}
