package com.prokarm.producer.service;

import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import com.prokarm.producer.dto.CustomerPublisher;
import com.prokarm.producer.exception.KafkaPublisherException;
import com.prokarm.producer.utils.JsonMapperUtil;
import com.prokarm.producer.utils.PublisherConstants;

@Component
public class KafkaPublisherService implements KafkaPublisher {

  private static final Logger logger = LoggerFactory.getLogger(KafkaPublisherService.class);

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  @Value("${prokarma.kafka.producer.topic}")
  private String topic;

  @Override
  public String publish(CustomerPublisher publisher) {
    String customerDetails = JsonMapperUtil.objectToJson(publisher.getCustomer());
    String response = PublisherConstants.SUCCESS;
    ListenableFuture<SendResult<String, String>> result =
        kafkaTemplate.send(topic, customerDetails);

    try {
      if (result.get() != null && result.get().getRecordMetadata() != null) {
        logger.info("Successfully send the customer details to kafka. Offset : {}",
            result.get().getRecordMetadata().offset());
      }
      else {
        throw new KafkaPublisherException(PublisherConstants.ERROR_KAFKA_PUBLISHER_EXCEPTION);
      }
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error("InterruptedException occurred. Reason : {}", e.getMessage());
      throw new KafkaPublisherException(PublisherConstants.ERROR_KAFKA_PUBLISHER_EXCEPTION);
    } catch (ExecutionException e) {
      logger.error("ExecutionException occurred. Reason : {}", e.getMessage());
      throw new KafkaPublisherException(PublisherConstants.ERROR_KAFKA_PUBLISHER_EXCEPTION);
    }
    kafkaTemplate.destroy();
    return response;
  }

}
