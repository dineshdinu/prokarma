package com.prokarm.producer.converters;

import com.prokarm.producer.dto.SuccessResponse;

public interface CustomerResponseConverter {
  public SuccessResponse convert(String result);
}
