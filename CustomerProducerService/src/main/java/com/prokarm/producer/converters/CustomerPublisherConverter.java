package com.prokarm.producer.converters;

import com.prokarm.producer.domain.Customer;
import com.prokarm.producer.dto.CustomerPublisher;

public interface CustomerPublisherConverter {
  public CustomerPublisher convert(Customer customer);
}
