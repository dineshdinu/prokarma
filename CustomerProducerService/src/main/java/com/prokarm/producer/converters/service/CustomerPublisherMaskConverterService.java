package com.prokarm.producer.converters.service;

import org.springframework.stereotype.Component;
import com.prokarm.producer.converters.CustomerPublisherMaskConverter;
import com.prokarm.producer.domain.Customer;

@Component
public class CustomerPublisherMaskConverterService implements CustomerPublisherMaskConverter{

  public Customer convert(Customer customer) {
    Customer maskedDataCustomer = new Customer();

    maskedDataCustomer.setFirstName(customer.getFirstName());
    maskedDataCustomer.setLastName(customer.getLastName());
    maskedDataCustomer.setBirthdate(customer.getBirthdate());
    maskedDataCustomer.setCountry(customer.getCountry());
    maskedDataCustomer.setCountryCode(customer.getCountryCode());
    maskedDataCustomer.setMobileNumber(customer.getMobileNumber());
    maskedDataCustomer.setCustomerStatus(customer.getCustomerStatus());
    maskedDataCustomer.setAddress(customer.getAddress());

    /*
     * Masking the customer sensitive details.
     */
    maskedDataCustomer.setCustomerNumber(customer.getCustomerNumber().replaceAll("[0-9A-z]{4}$", "****"));
    maskedDataCustomer.setEmail(customer.getEmail().replaceFirst("[0-9A-z]{4}", "****"));

    return maskedDataCustomer;
  }
}
