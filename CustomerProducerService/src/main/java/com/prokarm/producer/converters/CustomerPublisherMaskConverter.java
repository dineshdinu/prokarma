package com.prokarm.producer.converters;

import com.prokarm.producer.domain.Customer;

public interface CustomerPublisherMaskConverter {
  public Customer convert(Customer customer);
}
