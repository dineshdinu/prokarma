package com.prokarm.producer.converters.service;

import org.springframework.stereotype.Component;
import com.prokarm.producer.converters.CustomerResponseConverter;
import com.prokarm.producer.dto.SuccessResponse;
import com.prokarm.producer.utils.PublisherConstants;

@Component
public class CustomerResponseConverterService implements CustomerResponseConverter {
  public SuccessResponse convert(String result) {
    SuccessResponse response = new SuccessResponse();
    response.setStatus(PublisherConstants.SUCCESS);
    response.setMessage("Customer details successfully sent to kafka");
    return response;
  }
}
