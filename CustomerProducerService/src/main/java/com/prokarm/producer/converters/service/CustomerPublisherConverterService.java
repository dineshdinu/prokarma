package com.prokarm.producer.converters.service;

import org.springframework.stereotype.Component;
import com.prokarm.producer.converters.CustomerPublisherConverter;
import com.prokarm.producer.domain.Customer;
import com.prokarm.producer.dto.CustomerPublisher;

@Component
public class CustomerPublisherConverterService implements CustomerPublisherConverter{

    public CustomerPublisher convert(Customer customer) {
      CustomerPublisher customerPublisher = new CustomerPublisher();
      customerPublisher.setCustomer(customer);
      return customerPublisher;
    }
}
