package com.prokarm.producer.dto;

import java.time.LocalDate;

public class Customer {
  private String customerNumber;
  private String firstName;
  private String lastName;
  private LocalDate birthdate;
  private String country;
  private String countryCode;
  private Integer mobileNumber;
  private String email;
  private CustomerStatusEnum customerStatus;
  public enum CustomerStatusEnum {
    OPEN,
    CLOSE,
    SUSPENDED,
    RESTORED;
  }
  public String getCustomerNumber() {
    return customerNumber;
  }
  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public LocalDate getBirthdate() {
    return birthdate;
  }
  public void setBirthdate(LocalDate birthdate) {
    this.birthdate = birthdate;
  }
  public String getCountry() {
    return country;
  }
  public void setCountry(String country) {
    this.country = country;
  }
  public String getCountryCode() {
    return countryCode;
  }
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  public Integer getMobileNumber() {
    return mobileNumber;
  }
  public void setMobileNumber(Integer mobileNumber) {
    this.mobileNumber = mobileNumber;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public CustomerStatusEnum getCustomerStatus() {
    return customerStatus;
  }
  public void setCustomerStatus(CustomerStatusEnum customerStatus) {
    this.customerStatus = customerStatus;
  }
  
}
