package com.prokarm.producer.dto;

import com.prokarm.producer.domain.Customer;

public class CustomerPublisher   {
  private Customer customer = null;

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
  
}