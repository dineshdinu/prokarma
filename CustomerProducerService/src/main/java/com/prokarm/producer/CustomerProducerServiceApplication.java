package com.prokarm.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerProducerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerProducerServiceApplication.class, args);
	}

}
