package com.prokarm.producer.exception;

public class KafkaPublisherException extends RuntimeException{
  /**
   * 
   */
  private static final long serialVersionUID = -2005858487512648668L;

  public KafkaPublisherException(String message) {
    super(message);
  }
}
