package com.prokarm.producer.exception;

public class JsonParseException extends RuntimeException{
  /**
   * 
   */
  private static final long serialVersionUID = -2005858487512648668L;

  public JsonParseException(String message) {
    super(message);
  }
}
