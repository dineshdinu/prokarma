package com.prokarm.producer.exception;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import com.prokarm.producer.domain.ErrorResponse;
import com.prokarm.producer.utils.PublisherConstants;

@ControllerAdvice
public class CustomerControllerAdvice {

  @ExceptionHandler(AuthenticationException.class)
  public final ResponseEntity<ErrorResponse> handleAuthentication(AuthenticationException authenticationException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_AUTHENTICATION);
    errorResponse.setErrorType(AuthenticationException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
  }
  
  @ExceptionHandler(BindException.class)
  public final ResponseEntity<ErrorResponse> handleBind(BindException bindException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_VALIDATION);
    errorResponse.setErrorType(BindException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
  
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public final ResponseEntity<ErrorResponse> handleMethodArgumentNotValid(MethodArgumentNotValidException validationException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_VALIDATION);
    errorResponse.setErrorType(MethodArgumentNotValidException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
  
  @ExceptionHandler(InternalServerError.class)
  public final ResponseEntity<ErrorResponse> handleInternalServerError(InternalServerError serverException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_INTERNAL_SERVER_ERROR);
    errorResponse.setErrorType(InternalServerError.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @ExceptionHandler(NotFound.class)
  public final ResponseEntity<ErrorResponse> handleNotFound(NotFound notFoundException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_NOT_FOUND);
    errorResponse.setErrorType(NotFound.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
  }
  
  @ExceptionHandler(UnauthorizedUserException.class)
  public final ResponseEntity<ErrorResponse> handleUnauthorizedUserException(UnauthorizedUserException unAutherizedException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_AUTHENTICATION);
    errorResponse.setErrorType(UnauthorizedUserException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
  }
  
  @ExceptionHandler(KafkaPublisherException.class)
  public final ResponseEntity<ErrorResponse> handleKafkaPublisherException(KafkaPublisherException kafkaPublisherException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_KAFKA_PUBLISHER_EXCEPTION);
    errorResponse.setErrorType(KafkaPublisherException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @ExceptionHandler(JsonParseException.class)
  public final ResponseEntity<ErrorResponse> handleJsonParseException(JsonParseException jsonParseException,
      HttpServletRequest request) {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setStatus(PublisherConstants.ERROR);
    errorResponse.setMessage(PublisherConstants.ERROR_JSON_PARSE_EXCEPTION);
    errorResponse.setErrorType(JsonParseException.class.getSimpleName());
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
