package com.prokarm.consumer.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.prokarm.consumer.entities.ErrorLog;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
class ErrorLogRepoTest {
  
  @Autowired 
  private DataSource dataSource;
  
  @Autowired 
  private JdbcTemplate jdbcTemplate;
  
  @Autowired 
  private EntityManager entityManager;
  
  @Autowired 
  private ErrorLogRepo errorLogRepo;
  
  @Test
  void auditLogSaveTest() {
    ErrorLog savedLog = errorLogRepo.save(createErrorLog());
    assertThat(savedLog).isNotNull();
    assertEquals("DAOAccessException", savedLog.getErrorType());
  }

  @Test
  void injectedComponentsAreNotNull(){
    assertThat(dataSource).isNotNull();
    assertThat(jdbcTemplate).isNotNull();
    assertThat(entityManager).isNotNull();
    assertThat(errorLogRepo).isNotNull();
  }
  
  ErrorLog createErrorLog() {
   ErrorLog errorLog = new ErrorLog();
   errorLog.setCustomerPayload(customerpayload());
   errorLog.setErrorType("DAOAccessException");
   errorLog.setErrorDescription("Error while saving the object");
   return errorLog;
  }
  
  String customerpayload() {
      return "{\"customerNumber\":\"C098780979\",\"firstName\":\"ProkarmaProkarma\","
          + "\"lastName\":\"ProkarmaProkarma\",\"birthdate\":\"01-01-1993\","
          + " \"email\":\"prokarma@gmail.com\",\"country\":\"India\",\"countryCode\":\"IN\"}";
  }
}
