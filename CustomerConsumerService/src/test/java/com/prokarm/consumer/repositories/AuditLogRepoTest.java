package com.prokarm.consumer.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.prokarm.consumer.entities.AuditLog;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
class AuditLogRepoTest {
  
  @Autowired 
  private DataSource dataSource;
  
  @Autowired 
  private JdbcTemplate jdbcTemplate;
  
  @Autowired 
  private EntityManager entityManager;
  
  @Autowired 
  private AuditLogRepo auditLogRepo;
  
  @Test
  void auditLogSaveTest() {
    AuditLog savedLog = auditLogRepo.save(createAuditLog());
    assertThat(savedLog).isNotNull();
    assertEquals("C098780979", savedLog.getCustomerNumber());
  }

  @Test
  void injectedComponentsAreNotNull(){
    assertThat(dataSource).isNotNull();
    assertThat(jdbcTemplate).isNotNull();
    assertThat(entityManager).isNotNull();
    assertThat(auditLogRepo).isNotNull();
  }
  
  AuditLog createAuditLog() {
    AuditLog auditLog = new AuditLog();
    auditLog.setCustomerNumber("C098780979");
    auditLog.setCustomerPayload(customerpayload());
    return auditLog;
  }
  
  String customerpayload() {
      return "{\"customerNumber\":\"C098780979\",\"firstName\":\"ProkarmaProkarma\","
          + "\"lastName\":\"ProkarmaProkarma\",\"birthdate\":\"01-01-1993\","
          + " \"email\":\"prokarma@gmail.com\",\"country\":\"India\",\"countryCode\":\"IN\"}";
  }
}
