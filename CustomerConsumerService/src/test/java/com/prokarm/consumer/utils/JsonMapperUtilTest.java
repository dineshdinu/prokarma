package com.prokarm.consumer.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class JsonMapperUtilTest {

  @Test
  void testCustomerobjectFromJson() throws JsonProcessingException{
    assertNotNull(JsonMapperUtil.customerobjectFromJson(createCustomerString()));
  }

  String createCustomerString() {
    return "{\"customerNumber\":\"C098780979\",\"firstName\":\"ProkarmaProkarma\","
        + "\"lastName\":\"ProkarmaProkarma\",\"birthdate\":\"01-01-1993\","
        + " \"email\":\"prokarma@gmail.com\",\"country\":\"India\",\"countryCode\":\"IN\"}";
  }
}
