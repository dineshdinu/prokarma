package com.prokarm.consumer.service;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.prokarm.consumer.domains.Customer;
import com.prokarm.consumer.utils.JsonMapperUtil;

@ExtendWith(SpringExtension.class)
@EmbeddedKafka(partitions = 1, topics = {"customer_details"})
class KafkaConsumerServiceTest {

  @Autowired
  EmbeddedKafkaBroker embeddedKafkaBroker;

  @Test
  void testReceivingKafkaEvents() {
    Consumer<Integer, String> consumer = configureConsumer();
    Producer<Integer, String> producer = configureProducer();

    producer.send(new ProducerRecord<>("customer_details_test", 1, JsonMapperUtil.objectToJson(createCustomer())));

    ConsumerRecord<Integer, String> singleRecord =
        KafkaTestUtils.getSingleRecord(consumer, "customer_details_test");
    
    assertThat(singleRecord).isNotNull();
    assertThat(singleRecord.key()).isEqualTo(1);
    assertThat(singleRecord.value()).isNotNull();
    assertThat(singleRecord.value()).contains("Prokarma");

    consumer.close();
    producer.close();
  }

  private Consumer<Integer, String> configureConsumer() {
    Map<String, Object> consumerProps =
        KafkaTestUtils.consumerProps("testGroup", "true", embeddedKafkaBroker);
    consumerProps.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    Consumer<Integer, String> consumer =
        new DefaultKafkaConsumerFactory<Integer, String>(consumerProps).createConsumer();
    consumer.subscribe(Collections.singleton("customer_details_test"));
    return consumer;
  }

  private Producer<Integer, String> configureProducer() {
    Map<String, Object> producerProps =
        new HashMap<>(KafkaTestUtils.producerProps(embeddedKafkaBroker));
    return new DefaultKafkaProducerFactory<Integer, String>(producerProps).createProducer();
  }

  private Customer createCustomer() {
    Customer customer = new Customer();
    customer = new Customer();
    customer.setFirstName("Prokarma");
    customer.setCustomerNumber("CP345DD890");
    customer.setEmail("prokarma@gmail.com");
    customer.setCountryCode("IN");
    return customer;
  }

}
