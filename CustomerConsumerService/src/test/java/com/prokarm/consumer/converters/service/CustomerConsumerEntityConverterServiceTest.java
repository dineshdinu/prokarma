package com.prokarm.consumer.converters.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.prokarm.consumer.dto.Customer;
import com.prokarm.consumer.entities.AuditLog;
import com.prokarm.consumer.entities.ErrorLog;

@ExtendWith(MockitoExtension.class)
class CustomerConsumerEntityConverterServiceTest {
  CustomerConsumerEntityConverterService converter;
  Customer customer;

  @BeforeEach
  void setUp() {
    converter = new CustomerConsumerEntityConverterService();
    createCustomerDetails();
  }
  
  @Test
  void convertAuditLogTest() {
    AuditLog auditLog = converter.convertAuditLog(customer);
    assertEquals("CP345DD890", auditLog.getCustomerNumber());
  }
  
  @Test
  void convertErrorLogTest() {
    ErrorLog errorLog = converter.convertErrorLog(createCustomerString(),new NullPointerException());
    assertThat(errorLog.getCustomerPayload()).contains("Prokarma");
  }
  
  void createCustomerDetails() {
    customer = new Customer();
    customer.setFirstName("Prokarma");
    customer.setCustomerNumber("CP345DD890");
    customer.setEmail("prokarma@gmail.com");
    customer.setCountryCode("IN");
  }
  
  String createCustomerString() {
    return "{\"customerNumber\":\"C098780979\",\"firstName\":\"ProkarmaProkarma\","
        + "\"lastName\":\"ProkarmaProkarma\",\"birthdate\":\"01-01-1993\","
        + " \"email\":\"prokarma@gmail.com\",\"country\":\"India\",\"countryCode\":\"IN\"}";
  }
}
