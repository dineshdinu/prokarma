package com.prokarm.consumer.converters.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import com.prokarm.consumer.dto.Customer;

@ExtendWith(MockitoExtension.class)
class CustomerConsumerMaskConverterServiceTest {

  CustomerConsumerMaskConverterService converter;
  Customer customer;

  @BeforeEach
  void setUp() {
    converter = new CustomerConsumerMaskConverterService();
    createCustomerDetails();
  }

  @Test
  void testCustomerDetailsAreMasking() {
    Customer customerTest = converter.convert(customer);

    assertEquals("IN", customerTest.getCountryCode());
    assertEquals("CP345D****", customerTest.getCustomerNumber());
    assertEquals("****arma@gmail.com", customerTest.getEmail());
  }

  void createCustomerDetails() {
    customer = new Customer();
    customer.setFirstName("Prokarma");
    customer.setCustomerNumber("CP345DD890");
    customer.setEmail("prokarma@gmail.com");
    customer.setCountryCode("IN");
  }
}
