package com.prokarm.consumer.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import com.prokarm.consumer.converters.service.CustomerConsumerEntityConverterService;
import com.prokarm.consumer.dao.ErrorLogDao;
import com.prokarm.consumer.entities.ErrorLog;

@Aspect
public class CustomerConsumerAspect {
  
  @Autowired
  private ErrorLogDao errorLogDao;
  
  @Autowired
  private CustomerConsumerEntityConverterService entityConverter;
  
  @AfterThrowing(
      value = "execution(* com.prokarm.consumer.service.KafkaConsumerService.consume(..)))",
      throwing="exception"
      )
  public void captureErrorDetailsForConsumer(JoinPoint joinPoint, Exception exception) {
    String customerDetails = "";
    if(joinPoint.getArgs()[0] != null) {
      customerDetails = (String) joinPoint.getArgs()[0];
    }
    ErrorLog errorEntity = entityConverter.convertErrorLog(customerDetails, exception);
    errorLogDao.save(errorEntity);
  }
}
