package com.prokarm.consumer.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.prokarm.consumer.dao.ErrorLogDao;
import com.prokarm.consumer.entities.ErrorLog;
import com.prokarm.consumer.repositories.ErrorLogRepo;

@Repository
public class ErrorLogDaoService implements ErrorLogDao{

  @Autowired
  private ErrorLogRepo errorLogRepo;
  
  @Override
  public ErrorLog save(ErrorLog errorLog) {
    return errorLogRepo.save(errorLog);
  }

}
