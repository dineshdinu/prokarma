package com.prokarm.consumer.dao;

import com.prokarm.consumer.entities.ErrorLog;

public interface ErrorLogDao {
  public ErrorLog save(ErrorLog errorLog);
}
