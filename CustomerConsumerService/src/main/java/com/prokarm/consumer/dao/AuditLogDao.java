package com.prokarm.consumer.dao;

import com.prokarm.consumer.entities.AuditLog;

public interface AuditLogDao {
  public AuditLog save(AuditLog auditLog);
}
