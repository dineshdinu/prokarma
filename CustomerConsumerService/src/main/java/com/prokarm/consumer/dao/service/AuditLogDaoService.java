package com.prokarm.consumer.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.prokarm.consumer.dao.AuditLogDao;
import com.prokarm.consumer.entities.AuditLog;
import com.prokarm.consumer.repositories.AuditLogRepo;

@Repository
public class AuditLogDaoService implements AuditLogDao {
  
  @Autowired
  private AuditLogRepo auditLogRepo;
  
  @Override
  public AuditLog save(AuditLog auditLog) {
    return auditLogRepo.save(auditLog);
  }

}
