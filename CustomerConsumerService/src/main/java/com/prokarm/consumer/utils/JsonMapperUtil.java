package com.prokarm.consumer.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.prokarm.consumer.dto.Customer;
import com.prokarm.consumer.exception.JsonParseException;

public class JsonMapperUtil {
  
  private JsonMapperUtil() {}
  
  private static final Logger logger = LoggerFactory.getLogger(JsonMapperUtil.class);
  
  public static String objectToJson(Object sourceObject) {
      try {
          return new JsonMapper().writeValueAsString(sourceObject);
      } catch (JsonProcessingException e) {
        logger.error("Error while converting to json. cause : {}",e.getMessage());
        throw new JsonParseException(PublisherConstants.ERROR_JSON_PARSE_EXCEPTION);
      }
  }
  
  public static Customer customerobjectFromJson(String json) {
    try {
      return new JsonMapper().readValue(json, Customer.class);
    } catch (JsonProcessingException e) {
      logger.error("Error while converting to json. cause : {}",e.getMessage());
      throw new JsonParseException(PublisherConstants.ERROR_JSON_PARSE_EXCEPTION);
    }
  }
}
