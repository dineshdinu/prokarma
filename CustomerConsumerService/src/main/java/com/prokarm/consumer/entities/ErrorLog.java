package com.prokarm.consumer.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ERROR_LOG")
public class ErrorLog {
  
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private int id;
  
  @Column(name = "ERROR_TYPE")
  private String errorType;
  
  @Column(name = "ERROR_DESCRIPTION")
  private String errorDescription;
  
  @Column(name = "CUSTOMER_PAYLOAD")
  private String customerPayload;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getErrorType() {
    return errorType;
  }

  public void setErrorType(String errorType) {
    this.errorType = errorType;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

  public String getCustomerPayload() {
    return customerPayload;
  }

  public void setCustomerPayload(String customerPayload) {
    this.customerPayload = customerPayload;
  }

  
}
