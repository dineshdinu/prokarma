package com.prokarm.consumer.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUDIT_LOG")
public class AuditLog {
  
  @Id
  @GeneratedValue
  @Column(name = "ID")
  private int id;
  
  @Column(name = "CUSTOMER_NUMBER")
  private String customerNumber;
  
  @Column(name = "CUSTOMER_PAYLOAD")
  private String customerPayload;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getCustomerNumber() {
    return customerNumber;
  }
  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }
  public String getCustomerPayload() {
    return customerPayload;
  }
  public void setCustomerPayload(String customerPayload) {
    this.customerPayload = customerPayload;
  }
  
}
