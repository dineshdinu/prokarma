package com.prokarm.consumer.dto;

public class CustomerPublisher   {
  private Customer customer = null;

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
  
}