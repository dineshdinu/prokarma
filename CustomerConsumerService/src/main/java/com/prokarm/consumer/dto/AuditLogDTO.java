package com.prokarm.consumer.dto;

public class AuditLogDTO {
  private int id;
  private String customerNumber;
  private String customerPayload;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getCustomerNumber() {
    return customerNumber;
  }
  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }
  public String getCustomerPayload() {
    return customerPayload;
  }
  public void setCustomerPayload(String customerPayload) {
    this.customerPayload = customerPayload;
  }
}