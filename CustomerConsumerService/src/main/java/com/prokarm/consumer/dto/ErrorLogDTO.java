package com.prokarm.consumer.dto;

public class ErrorLogDTO {
  
  private int id;
  private String errorType;
  private String errorDescription;
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getErrorType() {
    return errorType;
  }
  public void setErrorType(String errorType) {
    this.errorType = errorType;
  }
  public String getErrorDescription() {
    return errorDescription;
  }
  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

}
