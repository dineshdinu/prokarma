package com.prokarm.consumer.service;

public interface KafkaConsumer {
  public void consume(String message);
}
