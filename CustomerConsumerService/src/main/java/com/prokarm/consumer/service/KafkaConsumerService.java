package com.prokarm.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.prokarm.consumer.converters.CustomerConsumerEntityConverter;
import com.prokarm.consumer.converters.CustomerConsumerMaskConverter;
import com.prokarm.consumer.dao.AuditLogDao;
import com.prokarm.consumer.dto.Customer;
import com.prokarm.consumer.entities.AuditLog;
import com.prokarm.consumer.utils.JsonMapperUtil;

@Service
public class KafkaConsumerService implements KafkaConsumer {

  private static final Logger logger = LoggerFactory.getLogger(KafkaConsumerService.class);

  public static final String TOPIC_NAME = "customer_details";
  
  @Autowired
  private AuditLogDao auditLogDao;
  
  @Autowired
  private CustomerConsumerEntityConverter entityConverter;
  
  @Autowired
  private CustomerConsumerMaskConverter maskConverter;

  @KafkaListener(topics = {TOPIC_NAME},groupId = "consumer_group")
  public void consume(String message) {
    Customer customer = JsonMapperUtil.customerobjectFromJson(message);
    String customerDetails = JsonMapperUtil.objectToJson(maskConverter.convert(customer));
    logger.info("customer details received : {}", customerDetails);
    if(!StringUtils.isEmpty(customer.getCustomerNumber())) {
      AuditLog auditLogEntity = entityConverter.convertAuditLog(customer);
      auditLogDao.save(auditLogEntity);
    }
  }

}
