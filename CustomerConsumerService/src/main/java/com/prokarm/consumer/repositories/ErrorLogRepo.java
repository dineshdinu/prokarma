package com.prokarm.consumer.repositories;

import org.springframework.data.repository.CrudRepository;
import com.prokarm.consumer.entities.ErrorLog;

public interface ErrorLogRepo extends CrudRepository<ErrorLog, Integer>{

}
