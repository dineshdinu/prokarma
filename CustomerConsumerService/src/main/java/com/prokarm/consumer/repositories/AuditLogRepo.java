package com.prokarm.consumer.repositories;

import org.springframework.data.repository.CrudRepository;
import com.prokarm.consumer.entities.AuditLog;

public interface AuditLogRepo extends CrudRepository<AuditLog, Integer> {

}
