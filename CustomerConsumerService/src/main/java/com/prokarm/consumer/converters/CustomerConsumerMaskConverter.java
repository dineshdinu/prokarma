package com.prokarm.consumer.converters;

import com.prokarm.consumer.dto.Customer;

public interface CustomerConsumerMaskConverter {
  public Customer convert(Customer customer);
}
