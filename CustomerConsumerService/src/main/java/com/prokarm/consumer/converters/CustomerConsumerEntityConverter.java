package com.prokarm.consumer.converters;

import com.prokarm.consumer.dto.Customer;
import com.prokarm.consumer.entities.AuditLog;
import com.prokarm.consumer.entities.ErrorLog;

public interface CustomerConsumerEntityConverter {
  public AuditLog convertAuditLog(Customer customer);
  public ErrorLog convertErrorLog(String customerPayLoad , Exception exception);
}
