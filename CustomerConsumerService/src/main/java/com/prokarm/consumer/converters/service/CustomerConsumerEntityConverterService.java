package com.prokarm.consumer.converters.service;

import org.springframework.stereotype.Component;
import com.prokarm.consumer.converters.CustomerConsumerEntityConverter;
import com.prokarm.consumer.dto.Customer;
import com.prokarm.consumer.entities.AuditLog;
import com.prokarm.consumer.entities.ErrorLog;
import com.prokarm.consumer.utils.JsonMapperUtil;

@Component
public class CustomerConsumerEntityConverterService implements CustomerConsumerEntityConverter{
  
  public AuditLog convertAuditLog(Customer customer) {
    AuditLog auditLog = new AuditLog();
    auditLog.setCustomerPayload(JsonMapperUtil.objectToJson(customer));
    auditLog.setCustomerNumber(customer.getCustomerNumber());
    return auditLog;
  }
  
  public ErrorLog convertErrorLog(String customerPayLoad , Exception exception) {
    ErrorLog errorLog = new ErrorLog();
    errorLog.setCustomerPayload(customerPayLoad);
    errorLog.setErrorType(exception.getClass().getSimpleName());
    errorLog.setErrorDescription(exception.getMessage());
    return errorLog;
  }
}
